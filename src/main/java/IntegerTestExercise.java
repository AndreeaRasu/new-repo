public class IntegerTestExercise {
    public static void main(String[] args) {
        int carSpeed;
        carSpeed = 37;

        System.out.println("Car is running at the speed of: " +  carSpeed);
        carSpeed = carSpeed + 55;

        System.out.println("Current speed of the car is: " +  carSpeed);

    }

}
