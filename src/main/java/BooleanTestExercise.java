public class BooleanTestExercise {

    public static void main(String[] args) {
        boolean testResult;

        testResult = true;

        System.out.println("Test Result is: " +  testResult);

        testResult = false;

        System.out.println("Test Result is: " +  testResult);
    }

}