package com.seleniumeasy.steps;

import com.seleniumeasy.tpages.GoogleCookiePage;
import com.seleniumeasy.tpages.GooglePage;
import com.seleniumeasy.tpages.GoogleResultPage;
import net.thucydides.core.annotations.Step;

public class GoogleSteps {

    private GooglePage googlePage;
    private GoogleResultPage googleResultPage;
    private GoogleCookiePage googleCookiePage;


    @Step
    public void navigate_to_google_page(){
        googlePage.open();
        googleCookiePage.clickAcceptCookieButton();
    }

    @Step
    public void user_searches_word(String value){
        googlePage.setSearchField(value);
        googlePage.clickGoogleSearchButton();
    }

    @Step
    public void assert_result_titles_contain(){
        System.out.println(   googleResultPage.getTextFirstResult() );

    }
}
