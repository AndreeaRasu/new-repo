package com.seleniumeasy.keywords;

import com.seleniumeasy.steps.GoogleSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;


public class GoogleKeywords {



    @Steps private GoogleSteps googleSteps;

    @Given("that the user is on the google page")
    public void thatTheUserIsOnTheGooglePage() {
        googleSteps.navigate_to_google_page();

    }

    @When("the user searches for {string}")
    public void theUserSearchesFor(String value) {
        googleSteps.user_searches_word(value);
    }

    @Then("relevant results are shown")
    public void relevantResultsAreShown() {
        googleSteps.assert_result_titles_contain();
    }
}
