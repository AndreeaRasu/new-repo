package com.seleniumeasy.tpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class GoogleCookiePage extends PageObject {

    @FindBy(css = "#introAgreeButton > span > span")
    private WebElementFacade acceptCookieButton;

    public void clickAcceptCookieButton(){
        waitABit(3000);
        getDriver().switchTo().frame("#cnsw > iframe");
        acceptCookieButton.click();
    }
}
