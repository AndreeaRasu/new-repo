package com.seleniumeasy.tpages;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("page:home.page")
public class GooglePage extends PageObject{

    @FindBy(name = "q")
    private WebElementFacade searchField;
    @FindBy(name = "btnK")
    private WebElementFacade googleSearchButton;

    public void setSearchField(String value){
        searchField.sendKeys(value);
    }

    public void clickGoogleSearchButton(){
        googleSearchButton.click();
    }



}
