package com.seleniumeasy.tpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class GoogleResultPage extends PageObject {

    @FindBy(css = "#rso > div:nth-child(*) > div > div.yuRUbf > a > h3 > span")
    private List<WebElementFacade> listOfResultTitles;

    public String getTextFirstResult(){
        return listOfResultTitles.get(0).getText();
    }

}
