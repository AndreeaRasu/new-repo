Feature: Google Search

  Scenario: As as user I want to be able to find relevant results when I use the search functionality
    Given that the user is on the google page
    When the user searches for "fast"
    Then relevant results are shown
